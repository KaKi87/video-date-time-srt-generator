#!/usr/bin/env node

const fs = require('fs');

const
    arg = require('arg'),
    exif = require('exiftool'),
    dayjs = require('dayjs'),
    outdent = require('outdent');

const {
    '--input': inputFilePath,
    '--format': format,
    '--locale': locale,
    '--timezone': timezone
} = arg({
    '--input': String,
    '--format': String,
    '--locale': String,
    '--timezone': String,
    '-i': '--input',
    '-f': '--format',
    '-l': '--locale',
    '-t': '--timezone'
});

dayjs.extend(require('dayjs/plugin/customParseFormat'));
dayjs.extend(require('dayjs/plugin/utc'));

if(locale){
    dayjs.locale(require(`dayjs/locale/${locale}`));
    dayjs.extend(require('dayjs/plugin/localizedFormat'));
    dayjs.locale(locale);
}


if(timezone){
    dayjs.extend(require('dayjs/plugin/timezone'));
}

fs.readFile(inputFilePath, (fsError, fsData) => {
    if(fsError) console.error(fsError);
    else exif.metadata(fsData, (exifError, exifData) => {
        if(exifError) console.error(exifError);
        else {
            const
                startDate = dayjs(exifData['trackCreateDate'], 'YYYY:MM:DD HH:mm:ss'),
                durationDate = dayjs(exifData['mediaDuration'], 'HH:mm:ss'),
                endDate = dayjs(startDate)
                    .add(durationDate.hour(), 'hours')
                    .add(durationDate.minute(), 'minutes')
                    .add(durationDate.second(), 'seconds');
            let srt = '';
            for(let i = 0; i < endDate.diff(startDate, 'seconds'); i++){
                let currentDate = dayjs(startDate).add(i, 'seconds');
                if(timezone) currentDate = currentDate.tz(timezone);
                if(i > 0) srt += '\n\n';
                srt += outdent `
                    ${i+1}
                    ${dayjs(0).add(i, 'seconds').utc().format('HH:mm:ss,SSS')} --> ${dayjs(0).add(i+1, 'seconds').utc().format('HH:mm:ss,SSS')}
                    ${currentDate.format(format)}
                `;
            }
            console.log(srt);
        }
    });
});